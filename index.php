<?php
spl_autoload_register(function ($nombre_clase) {
    include $nombre_clase . '.php';
});

$a=new Numeros([1,2,3]);
//var_dump($a);
//echo $a->valores; es una propiedad privada, es un fatal error
echo $a->getValores();
echo $a->getValores1();

echo $a->sumar1();
echo $a->media();


echo $a->setValores([44,55,6,7])
        ->getValores1();
/*
echo $a->sumar();

echo $a->sumar1();

echo $a->media();
 

echo $a->producto1();
echo $a->producto();
 * 
 */

echo $a->getSuma();
echo $a->getMedia();
echo $a->getProducto();
echo Numeros::sumaEstatica([2,3,4]);